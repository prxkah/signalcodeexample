namespace Tests.SignalCodeHandling.Models
{
    public class PresenceHisResponse : ISignalCode
    {
        public string UT1_SIGNKOD { get; set; }
        public string EmployerCode { get; set; }
        public string UnitName { get; set; }
    }
}