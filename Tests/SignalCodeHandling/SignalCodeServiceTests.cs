using FluentAssertions;
using NUnit.Framework;
using System.IO;
using Tests.Configurations;
using Tests.SignalCodeHandling.Models;

namespace Tests.SignalCodeHandling
{
    public class SignalCodeServiceTests
    {
        private ISignalCodeService _sut;

        [SetUp]
        public void Setup()
        {
            var configurationFolder = Path.Combine(TestContext.CurrentContext.TestDirectory, "configurations");
            var configuration = ConfigurationsCreator.GetJsonConfigurationsInFolder(configurationFolder);

            _sut = new SignalCodeService(configuration);
        }

        [Test]
        public void Should_ReplaceOneToken()
        {
            var response = new PresenceHisResponse
            {
                UT1_SIGNKOD = "0010",
                EmployerCode = "100",
                UnitName = "Blåa avdelningen"
            };

            var message = _sut.ExtractMessage<PresenceHisResponse>(response);

            message.Should().Be("Namn på avdelning: Blåa avdelningen");
        }

        [Test]
        public void Should_ReplaceMultipleTokens()
        {
            var response = new PresenceHisResponse
            {
                UT1_SIGNKOD = "0020",
                UnitName = "Blåa avdelningen"
            };

            var message = _sut.ExtractMessage<PresenceHisResponse>(response);

            message.Should().Be("Avdelning: Blåa avdelningen. Arbetsgivarkod: 100");
        }

        [Test]
        public void Should_GiveEmptyStringWhenSignalCodeIsMissing()
        {
            var response = new PresenceHisResponse
            {
                UT1_SIGNKOD = "0000"
            };

            var message = _sut.ExtractMessage<PresenceHisResponse>(response);

            message.Should().Be(string.Empty);
        }
    }
}