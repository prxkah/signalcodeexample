using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Tests.SignalCodeHandling
{
    public interface ISignalCode
    {
        string UT1_SIGNKOD { get; set; }
    }

    public interface ISignalCodeService
    {
        string ExtractMessage<T>(T obj) where T : ISignalCode;
    }

    public class SignalCodeService : ISignalCodeService
    {
        private readonly IConfiguration _configuration;

        public SignalCodeService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string ExtractMessage<T>(T obj) where T : ISignalCode
        {
            var message = GetRawMessage(obj);

            var propertyNameToValueMap = obj.GetType()
                .GetProperties()
                .Where(x => message.Contains($"<{x.Name}>"))
                .ToDictionary(x => x.Name, x => x.GetValue(obj).ToString());

            foreach (var key in propertyNameToValueMap.Keys)
            {
                message = message.Replace($"<{key}>", propertyNameToValueMap[key]);
            }

            return message;
        }

        private string GetRawMessage<T>(T obj) where T : ISignalCode
        {
            var sectionName = typeof(T).Name;
            var signalCode = obj.UT1_SIGNKOD;

            var signalCodeToMessageMap = _configuration
                .GetSection(sectionName)
                .Get<Dictionary<string, string>>();

            return signalCodeToMessageMap.ContainsKey(signalCode)
                ? signalCodeToMessageMap[signalCode]
                : string.Empty;
        }
    }
}