using System.IO;
using Microsoft.Extensions.Configuration;

namespace Tests.Configurations
{
    public static class ConfigurationsCreator
    {
        public static IConfiguration GetJsonConfigurationsInFolder(string folderPath)
        {
            var builder = new ConfigurationBuilder().SetBasePath(folderPath);

            var files = Directory.GetFiles(folderPath);

            foreach (var file in files)
            {
                builder.AddJsonFile(file);
            }

            return builder.Build();
        }
    }
}